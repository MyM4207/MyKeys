/*
  WiFi telen Server

 A simple telenet server takes character in entry and reply with internal states.
 
 Change the macro WIFI_AP, WIFI_PASSWORD and WIFI_AUTH accordingly.

The server disconnect automatically the client after 60s of inactivity.

TODO:
- put every character in a string and use strcmp, etc...
- use telent to light up led  (done) 
- p: to print the GPS position (done)
- h: for the help (done)
- n: to enter the name or student number (done)
- save information to flash memory (weired)
- date (weired, january 2004)

 */
#include <LWiFi.h>
#include <LWiFiServer.h>
#include <LWiFiClient.h>
#include <LGPS.h>
#include <LDateTime.h>
#include <LFlash.h>
#include <LSD.h>
#include <LStorage.h>
#include <Wire.h>
#include "rgb_lcd.h"

#define WIFI_AP "LinkitOneWifi"
#define WIFI_PASSWORD ""
#define WIFI_AUTH LWIFI_OPEN  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP according to your WiFi AP configuration

#define LED 13

#define Drv LFlash 

#define ROTARY_ANGLE_SENSOR A0
#define ADC_REF 5//reference voltage of ADC is 5v.If the Vcc switch on the seeeduino
                     //board switches to 3V3, the ADC_REF should be 3.3
#define GROVE_VCC 5//VCC of the grove interface is normally 5v
#define FULL_ANGLE 300//full value of the rotary angle is 300 degrees
#define BUTTON A1
#define LENGHT 8



rgb_lcd lcd;

const int colorR = 255;
const int colorG = 0;
const int colorB = 0;


int buttonState = 0;
int degrees;
int number[LENGHT]={0,0,0,0,0,0,0,0};
int pos = 0;
int i = 0;

LWiFiServer server(23);
gpsSentenceInfoStruct info;

char buff[256];
int ledStatus = 0;
int inactiveDelay = 0;

int tentative = 0; 
int maxretry = 10; 
byte bssid[6];
int j = 0;
int retval = 0;


unsigned int rtc;


LWiFiClient client;

void setup()
{
  pinMode(LED,OUTPUT);
  digitalWrite(LED,LOW);
  pinMode(ROTARY_ANGLE_SENSOR, INPUT);
  pinMode(BUTTON, INPUT);

  LWiFi.begin();
  Serial.begin(115200);

  // Start LCD... 
  lcd.begin(16, 2);
  lcd.setRGB(colorR, colorG, colorB);
  lcd.print("Numero etudiant:");
  delay(1000);
  

  /* Below, I try to list all the AP and try to get their
   *  mac address. To do so, you have to connect to the 
   *  the AP itself. There is no way to get the MAC address
   *  without connecting to the AP. GRRR
  */ 
  getRSSIandMAC("eduspot",LWIFI_OPEN, "");
  getRSSIandMAC("RT-WIFI-Guest",LWIFI_WPA, "wifirtguest" );
  
  // keep retrying until connected to AP
  Serial.println("Connecting to AP");
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
  {
    delay(1000);
  }

  printWifiStatus();

  Serial.println("Start Server");
  server.begin();
  Serial.println("Server Started");

  LGPS.powerOn();
  Serial.println("LGPS Power on, and waiting ..."); 
  delay(2000);

  // flash writing, from example
  pinMode(10, OUTPUT);
  Drv.begin();
  Serial.println("card initialized.");
}

int loopCount = 0;
bool enterName = false;
LFile dataFile;

void loop()
{
  // put your main code here, to run repeatedly:
  delay(500);
  //LDateTime.getTime(&t);
  LDateTime.getRtc(&rtc);
  //Serial.print("Epoch time is: ");
  //Serial.println(rtc);

  // BEGIN Mnage LCD and Student Number
  // The way of putting student number in mutual exclusive with
  // the telnet connection. When the Client is disconnected, you can do it again
  buttonState = analogRead(BUTTON) ;
  if (pos < LENGHT){ 
    if (buttonState > 100) {
      number[pos] = map(degrees, 0, FULL_ANGLE, 0, 9); 
      pos = pos + 1;
      
      // do not exit until button is released
      while(buttonState > 100){
        delay(200);
        buttonState = analogRead(BUTTON) ;
      }   
    } else {
    degrees = getDegree();
    lcd.setCursor(pos, 1);
    lcd.print(map(degrees, 0, FULL_ANGLE, 0, 9));
    lcd.setCursor(0, 1);
    lcd.cursor();
    }
  } else {
    // Sudent number is fully given... send it or store it
    // then set pos to 0 so that we can reset and/or enter another student number  
  }
  placeCursor();
  // END Mnage LCD and Student Number
  
  
  
  
  
  loopCount++;
  // la ligne suivante: LWiFiClient client = server.available() est non bloquante
  // Attention, 1 client à la fois.
  // Create an inactive timeout
  // This means that when no client is connected you can do other stuffs... which is good
  // Use LED if client is connected 
  client = server.available();
  if (client)
  {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    printBackHelp(client);
    /* The while block below automatically disconnect a client 
     *  after 60 seconds of inactivity. Inactivity means that 
     *  no character is sent through telnet.
    */
    while (client.connected())
    {
      inactiveDelay ++ ;
      if (inactiveDelay > 6000 ) {
        Serial.println("close connection inactive delay");
        client.stop();
        inactiveDelay = 0;
      }
      delay(10);

      /* The IF below, is used to handle each character. and
       *  execute and action depending on each character.
      */
      if (client.available())
      {
        // we basically process each caractere at a time
        int c = client.read();
        inactiveDelay = 0;

        Serial.print((char)c);
        if (c == 'h'){
          printBackHelp(client);
        }

        if (c == 'l'){
          if (ledStatus == 0 ){
            digitalWrite(LED,HIGH);
            ledStatus = 1 ;
          } else {
            digitalWrite(LED,LOW);
            ledStatus = 0;
          }
        }

        if (c == 'n'){
           // getStudentNumber(client);
           client.println("Please use the button/LCD/Rotary");
        }

        if (c == 'd') {
            Serial.println("opening file: ");
            dataFile = Drv.open("datalog.txt");
            if (dataFile) {
              dataFile.seek(0);
              while (dataFile.available()) {            
                Serial.write(dataFile.read());
              }
              Serial.println("reading ok");
              dataFile.close();
            } else {
              Serial.println("Error reading file ...");
            }
        }
        
        if (c == 'w'){
          printBackWifiStatus(client);
        }
        
        if (c == 'p'){
          client.println("You are going to be disconnected ...");
          client.println("Reconnect after ... 30s");
          client.stop();
          server.end();
          getWiFiPosition(client);
          server.begin();
          client = server.available();
          Serial.println("Server re-started");
        }
                    
        if (c == 'g'){
          printBackGPS(client);
        }

        if (c == 'q'){
            client.println("Bye");
            client.println("");
            Serial.println("close connection");
            client.stop();
            Serial.println("client disconnected");
        }

        if (c == '\n')
        {
          // you're starting a new line
          currentLineIsBlank = true;
        }
        else if (c != '\r')
        {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the client time to receive the data
    delay(500);
  }

  // 

}

void printWifiStatus()
{
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());

  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());

  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void printBackHelp(LWiFiClient client)
{
  client.println("");
  client.println("");  
  client.println("Disconnect with q when you're finished");
  client.println("Type h for help");
  client.println("");
  client.println("");
  client.println("List of available commands");
  client.println("\t h: to print this help");
  client.println("\t w: to print the wifi status");
  client.println("\t g: to print the current gps position");
  client.println("\t p: to print key position based on WiFi");
  client.println("\t l: toggle internal led");
  client.println("\t d: show data log");
  client.println("\t n: enter your student number");
  client.println("\t q: to print close the connection");
  client.println("");
}

void printBackGPS(LWiFiClient client)
{
  LGPS.getData(&info);
  client.println((char*)info.GPGGA);
  client.println("");
}

void printBackWifiStatus(LWiFiClient client)
{
  // print the SSID of the network you're attached to:
  client.print("SSID: ");
  client.println(LWiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  client.print("IP Address: ");
  client.println(ip);

  client.print("subnet mask: ");
  client.println(LWiFi.subnetMask());

  client.print("gateway IP: ");
  client.println(LWiFi.gatewayIP());

  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  client.print("signal strength (RSSI):");
  client.print(rssi);
  client.println(" dBm");
  client.println("");
}

void getStudentNumber(LWiFiClient client){
          int index = 0;
          int saisie = 1;
          char num[255];
          String dataString = "";
          client.print("saisir votre numero etudiant: ");
          char cc = client.read();
          while ( saisie == 1 ){
            //Serial.print(".");
            //Serial.print(cc);
            num[index] = cc;
            index ++;
            cc = client.read();
            dataString += String(cc);
            if (cc == '#'){
              saisie = 0;
              //Serial.println("change saisie");
            }
            delay(1000);
          }
          num[index+1] = '\0';
          Serial.println(dataString);
          client.println(dataString);
          /* Ne fontionne pas ...  */
          dataFile = Drv.open("datalog.txt", FILE_WRITE);
          if (dataFile) {
            dataFile.print("numero etudiant: ");
            dataFile.println(num);
            dataFile.close();
            Serial.println("writing ok");
          
          } else {
            Serial.println("Error opening file ...");
          }  
}

void reconnectToWiFi(char *n, LWiFiEncryption auth, char *p){
   retval = 0;
   tentative = 0;
   LWiFi.disconnect(); 
   delay(3000);
   Serial.print("Connecting to : ");
   Serial.print(n);
   retval = LWiFi.connect( n, LWiFiLoginInfo(auth, p));
   while (retval == 0 ){
        delay(1000);
        retval = LWiFi.connect( n, LWiFiLoginInfo(auth, p));
  }
  //Serial.println("");
  printWifiStatus();
}

void getRSSIandMAC(char *n, LWiFiEncryption auth, char *p){
   retval = 0;
   tentative = 0;
   char myssid[18];
   LWiFi.disconnect(); 
   delay(3000);
   retval = LWiFi.connect( n, LWiFiLoginInfo(auth, p));
   while (tentative < maxretry && retval == 0 ){
        delay(1000);
        Serial.print(".");
        retval = LWiFi.connect( n, LWiFiLoginInfo(auth, p));
        tentative ++; 
  }
  Serial.println("");
  if (tentative < maxretry ) {
    Serial.print(n);
    Serial.print(" // RSSI is: ");
    Serial.print(LWiFi.RSSI());
    Serial.print(" dBm, MAC is: ");
    LWiFi.BSSID(bssid);
    sprintf(myssid,"%02X:%02X:%02X:%02X:%02X:%02X",
      bssid[5],bssid[4],bssid[3],bssid[2],bssid[1],bssid[0] );
    }
    
    Serial.print(myssid);
    LWiFi.disconnect(); 
    delay(3000);
    Serial.println("");
}

void getWiFiPosition(LWiFiClient client){

  getRSSIandMAC("eduspot",LWIFI_OPEN, "");
  getRSSIandMAC("RT-WIFI-Guest",LWIFI_WPA, "wifirtguest" );
  reconnectToWiFi("LinkitOneWifi",LWIFI_OPEN, "" );
  // send data to some web server

}

void placeCursor(){
  // BEGIN Place cursor
  for (i = 0; i < pos; i ++) {
        lcd.setCursor(i, 1);
        lcd.print(number[i]);
        lcd.setCursor(i+1, 1);
  }
  for (i = pos +1; i < LENGHT; i ++) {
        lcd.setCursor(i, 1);
        lcd.print(number[i]);
        lcd.setCursor(i+1, 1);
  }  
  lcd.setCursor(pos, 1);
  lcd.cursor();
  // END Place cursor  
}

int getDegree() {
  int sensor_value = analogRead(ROTARY_ANGLE_SENSOR);
  float voltage;
  voltage = (float)sensor_value*ADC_REF/1023;
  float degrees = (voltage*FULL_ANGLE)/GROVE_VCC;
  return degrees;
}

