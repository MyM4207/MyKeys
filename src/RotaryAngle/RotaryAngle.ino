#include <Wire.h>
#include "rgb_lcd.h"


#define ROTARY_ANGLE_SENSOR A0
#define ADC_REF 5//reference voltage of ADC is 5v.If the Vcc switch on the seeeduino
                     //board switches to 3V3, the ADC_REF should be 3.3
#define GROVE_VCC 5//VCC of the grove interface is normally 5v
#define FULL_ANGLE 300//full value of the rotary angle is 300 degrees
#define BUTTON A1
#define LENGHT 8



rgb_lcd lcd;

const int colorR = 255;
const int colorG = 0;
const int colorB = 0;


int buttonState = 0;
int degrees;
int number[LENGHT]={0,0,0,0,0,0,0,0};
int pos = 0;
int i = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(ROTARY_ANGLE_SENSOR, INPUT);
  pinMode(BUTTON, INPUT);
  Serial.begin(9600);

  lcd.begin(16, 2);
  lcd.setRGB(colorR, colorG, colorB);
  //lcd.print("hello, world!");
  lcd.print("Numero etudiant:");
  delay(1000);

}

void loop() {
  // put your main code here, to run repeatedly:
  buttonState = analogRead(BUTTON) ;
  if (pos < LENGHT){ 
    if (buttonState > 100) {
      number[pos] = map(degrees, 0, FULL_ANGLE, 0, 9); 
      pos = pos + 1;
      
      // do not exit until button is released
      while(buttonState > 100){
        delay(200);
        buttonState = analogRead(BUTTON) ;
      }   
    } else {
    degrees = getDegree();
    lcd.setCursor(pos, 1);
    lcd.print(map(degrees, 0, FULL_ANGLE, 0, 9));
    lcd.setCursor(0, 1);
    lcd.cursor();
    }
  } else {
    // Sudent number is fully given... send it or store it
    // then set pos to 0 so that we can reset and/or enter another student number  
  }
  placeCursor();
  delay(300);
  

}

void placeCursor(){
  // BEGIN Place cursor
  for (i = 0; i < pos; i ++) {
        lcd.setCursor(i, 1);
        lcd.print(number[i]);
        lcd.setCursor(i+1, 1);
  }
  for (i = pos +1; i < LENGHT; i ++) {
        lcd.setCursor(i, 1);
        lcd.print(number[i]);
        lcd.setCursor(i+1, 1);
  }  
  lcd.setCursor(pos, 1);
  lcd.cursor();
  // END Place cursor  
}

int getDegree() {
  int sensor_value = analogRead(ROTARY_ANGLE_SENSOR);
  float voltage;
  voltage = (float)sensor_value*ADC_REF/1023;
  float degrees = (voltage*FULL_ANGLE)/GROVE_VCC;
  return degrees;
}